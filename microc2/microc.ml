type action = Interpret | Semantic

let _ =
  let action = List.assoc Sys.argv.(1) [ ("-i", Interpret);("-s", Semantic) ] in
  let lexbuf = Lexing.from_channel stdin in
  let program = Parser.program Scanner.token lexbuf in
  match action with
      Interpret -> ignore (Interpret.run program)
    | Semantic -> ignore (Semantic.check_semantic program)
 
