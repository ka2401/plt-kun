type token =
  | SEMI
  | LPAREN
  | RPAREN
  | LBRACK
  | RBRACK
  | LBRACE
  | RBRACE
  | COMMA
  | PLUS
  | MINUS
  | TIMES
  | DIVIDE
  | ASSIGN
  | EQ
  | NEQ
  | LT
  | LEQ
  | GT
  | GEQ
  | RETURN
  | IF
  | ELSE
  | FOR
  | WHILE
  | INT
  | POINT
  | CURVE
  | DOT
  | TRANS
  | LAYER
  | ADDTOLAYER
  | VOID
  | LITERAL of (int)
  | ID of (string)
  | EOF

open Parsing;;
let _ = parse_error;;
# 1 "parser.mly"
 open Ast 
# 43 "parser.ml"
let yytransl_const = [|
  257 (* SEMI *);
  258 (* LPAREN *);
  259 (* RPAREN *);
  260 (* LBRACK *);
  261 (* RBRACK *);
  262 (* LBRACE *);
  263 (* RBRACE *);
  264 (* COMMA *);
  265 (* PLUS *);
  266 (* MINUS *);
  267 (* TIMES *);
  268 (* DIVIDE *);
  269 (* ASSIGN *);
  270 (* EQ *);
  271 (* NEQ *);
  272 (* LT *);
  273 (* LEQ *);
  274 (* GT *);
  275 (* GEQ *);
  276 (* RETURN *);
  277 (* IF *);
  278 (* ELSE *);
  279 (* FOR *);
  280 (* WHILE *);
  281 (* INT *);
  282 (* POINT *);
  283 (* CURVE *);
  284 (* DOT *);
  285 (* TRANS *);
  286 (* LAYER *);
  287 (* ADDTOLAYER *);
  288 (* VOID *);
    0 (* EOF *);
    0|]

let yytransl_block = [|
  289 (* LITERAL *);
  290 (* ID *);
    0|]

let yylhs = "\255\255\
\001\000\001\000\001\000\003\000\003\000\003\000\003\000\003\000\
\004\000\004\000\007\000\007\000\008\000\008\000\008\000\008\000\
\005\000\005\000\002\000\002\000\002\000\002\000\006\000\006\000\
\009\000\009\000\009\000\009\000\009\000\009\000\009\000\011\000\
\011\000\010\000\010\000\010\000\010\000\010\000\010\000\010\000\
\010\000\010\000\010\000\010\000\010\000\010\000\010\000\010\000\
\010\000\010\000\010\000\010\000\010\000\010\000\010\000\010\000\
\013\000\013\000\014\000\014\000\012\000\012\000\015\000\015\000\
\000\000"

let yylen = "\002\000\
\000\000\002\000\002\000\009\000\009\000\009\000\009\000\009\000\
\000\000\001\000\001\000\003\000\002\000\002\000\002\000\002\000\
\000\000\002\000\003\000\003\000\003\000\003\000\000\000\002\000\
\002\000\003\000\003\000\005\000\007\000\009\000\005\000\000\000\
\001\000\001\000\006\000\020\000\005\000\009\000\003\000\003\000\
\003\000\001\000\003\000\003\000\003\000\003\000\002\000\003\000\
\003\000\003\000\003\000\003\000\003\000\003\000\004\000\003\000\
\000\000\001\000\001\000\003\000\000\000\001\000\001\000\003\000\
\002\000"

let yydefred = "\000\000\
\001\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\002\000\003\000\000\000\000\000\000\000\000\000\000\000\019\000\
\000\000\020\000\000\000\021\000\000\000\022\000\000\000\000\000\
\000\000\000\000\000\000\000\000\000\000\000\000\011\000\000\000\
\000\000\000\000\000\000\013\000\014\000\015\000\016\000\000\000\
\000\000\000\000\000\000\000\000\000\000\017\000\012\000\017\000\
\017\000\017\000\017\000\000\000\000\000\000\000\000\000\000\000\
\000\000\000\000\000\000\000\000\018\000\000\000\000\000\000\000\
\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\023\000\004\000\000\000\000\000\000\000\000\000\000\000\034\000\
\000\000\024\000\000\000\005\000\006\000\007\000\008\000\000\000\
\063\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\000\000\000\000\000\000\000\000\000\000\025\000\000\000\000\000\
\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\000\000\056\000\000\000\040\000\000\000\027\000\026\000\000\000\
\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\041\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\000\000\000\000\000\000\039\000\000\000\064\000\000\000\000\000\
\000\000\055\000\000\000\000\000\000\000\000\000\000\000\000\000\
\031\000\000\000\000\000\000\000\000\000\000\000\000\000\035\000\
\000\000\000\000\029\000\000\000\000\000\000\000\000\000\000\000\
\038\000\030\000\000\000\000\000\000\000\000\000\000\000\000\000\
\000\000\000\000\000\000\000\000\036\000"

let yydgoto = "\002\000\
\003\000\061\000\010\000\029\000\052\000\062\000\030\000\031\000\
\082\000\083\000\122\000\090\000\125\000\126\000\091\000"

let yysindex = "\012\000\
\000\000\000\000\025\000\252\254\019\255\022\255\026\255\029\255\
\000\000\000\000\021\255\120\255\127\255\160\255\068\255\000\000\
\241\255\000\000\241\255\000\000\241\255\000\000\241\255\241\255\
\049\255\052\255\062\255\064\255\097\255\107\255\000\000\108\255\
\122\255\132\255\139\255\000\000\000\000\000\000\000\000\126\255\
\241\255\140\255\142\255\145\255\149\255\000\000\000\000\000\000\
\000\000\000\000\000\000\006\000\006\000\006\000\006\000\006\000\
\125\255\131\255\134\255\135\255\000\000\041\255\048\255\103\255\
\110\255\143\255\171\255\174\255\181\255\187\255\085\255\152\255\
\000\000\000\000\085\255\085\255\189\255\193\255\197\255\000\000\
\018\255\000\000\008\000\000\000\000\000\000\000\000\000\051\000\
\000\000\196\255\194\255\150\255\055\255\029\000\085\255\085\255\
\085\255\085\255\085\255\175\255\178\255\000\000\085\255\085\255\
\085\255\085\255\085\255\085\255\085\255\085\255\085\255\085\255\
\085\255\000\000\085\255\000\000\199\255\000\000\000\000\094\000\
\044\001\204\255\115\000\044\001\205\255\207\255\044\001\217\255\
\000\000\055\255\055\255\192\255\192\255\058\001\058\001\129\255\
\129\255\129\255\129\255\000\000\073\000\000\000\216\255\085\255\
\216\255\000\000\085\255\085\255\223\255\085\255\212\255\237\255\
\000\000\044\001\136\000\085\255\236\000\216\255\085\255\000\000\
\252\000\085\255\000\000\240\255\085\255\157\000\216\255\178\000\
\000\000\000\000\245\255\085\255\012\001\085\255\199\000\246\255\
\085\255\028\001\085\255\220\000\000\000"

let yyrindex = "\000\000\
\000\000\000\000\245\000\000\000\000\000\000\000\000\000\000\000\
\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\253\255\000\000\253\255\000\000\253\255\000\000\253\255\253\255\
\000\000\000\000\000\000\000\000\000\000\004\000\000\000\000\000\
\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\000\000\000\000\000\000\183\255\183\255\183\255\183\255\183\255\
\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\000\000\000\000\000\000\000\000\000\000\000\000\000\000\003\000\
\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\000\255\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\000\000\000\000\009\000\000\000\118\001\000\000\000\000\020\000\
\000\000\010\000\000\000\000\000\000\000\000\000\000\000\000\000\
\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\056\255\000\000\000\000\255\254\000\000\026\000\089\255\000\000\
\000\000\137\001\156\001\080\001\099\001\227\255\251\001\175\001\
\194\001\213\001\232\001\000\000\000\000\000\000\000\000\020\000\
\000\000\000\000\000\000\000\000\243\255\000\000\190\255\000\000\
\000\000\096\255\000\000\000\000\000\000\000\000\031\000\000\000\
\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\000\000\000\000\000\000\000\000\000\000"

let yygindex = "\000\000\
\000\000\025\001\000\000\208\000\023\001\125\000\000\000\250\000\
\191\255\185\255\118\255\000\000\000\000\000\000\000\000"

let yytablesize = 778
let yytable = "\088\000\
\042\000\059\000\042\000\093\000\094\000\152\000\059\000\042\000\
\042\000\042\000\042\000\042\000\001\000\042\000\042\000\042\000\
\042\000\042\000\042\000\098\000\164\000\016\000\017\000\120\000\
\121\000\123\000\124\000\127\000\042\000\011\000\099\000\130\000\
\131\000\132\000\133\000\134\000\135\000\136\000\137\000\138\000\
\139\000\140\000\071\000\141\000\072\000\100\000\073\000\074\000\
\101\000\071\000\075\000\072\000\012\000\073\000\084\000\013\000\
\033\000\075\000\033\000\014\000\076\000\077\000\015\000\078\000\
\079\000\105\000\106\000\076\000\077\000\024\000\078\000\079\000\
\121\000\080\000\081\000\154\000\155\000\151\000\157\000\153\000\
\080\000\081\000\036\000\113\000\161\000\037\000\071\000\121\000\
\072\000\054\000\166\000\054\000\163\000\168\000\075\000\038\000\
\054\000\039\000\060\000\040\000\173\000\170\000\175\000\060\000\
\071\000\178\000\072\000\180\000\073\000\085\000\042\000\071\000\
\075\000\072\000\041\000\073\000\086\000\080\000\081\000\075\000\
\018\000\019\000\076\000\077\000\043\000\078\000\079\000\020\000\
\021\000\076\000\077\000\046\000\078\000\079\000\044\000\080\000\
\081\000\103\000\104\000\105\000\106\000\045\000\080\000\081\000\
\071\000\048\000\072\000\049\000\073\000\087\000\050\000\071\000\
\075\000\072\000\051\000\073\000\118\000\113\000\067\000\075\000\
\022\000\023\000\076\000\077\000\068\000\078\000\079\000\069\000\
\070\000\076\000\077\000\016\000\078\000\079\000\018\000\080\000\
\081\000\063\000\064\000\065\000\066\000\020\000\080\000\081\000\
\023\000\089\000\023\000\022\000\023\000\023\000\095\000\028\000\
\023\000\028\000\096\000\028\000\028\000\092\000\097\000\028\000\
\116\000\117\000\023\000\023\000\144\000\023\000\023\000\146\000\
\128\000\028\000\028\000\129\000\028\000\028\000\147\000\023\000\
\023\000\071\000\148\000\072\000\113\000\073\000\028\000\028\000\
\156\000\075\000\032\000\048\000\033\000\048\000\034\000\035\000\
\142\000\158\000\048\000\076\000\077\000\159\000\078\000\079\000\
\048\000\048\000\167\000\037\000\065\000\037\000\172\000\177\000\
\080\000\081\000\037\000\037\000\037\000\037\000\037\000\009\000\
\037\000\037\000\037\000\037\000\037\000\037\000\010\000\061\000\
\102\000\025\000\026\000\027\000\057\000\062\000\028\000\037\000\
\103\000\104\000\105\000\106\000\032\000\107\000\108\000\109\000\
\110\000\111\000\112\000\009\000\058\000\119\000\057\000\058\000\
\059\000\032\000\047\000\060\000\113\000\103\000\104\000\105\000\
\106\000\000\000\107\000\108\000\109\000\110\000\111\000\112\000\
\000\000\004\000\005\000\006\000\000\000\114\000\007\000\000\000\
\008\000\113\000\115\000\103\000\104\000\105\000\106\000\000\000\
\107\000\108\000\109\000\110\000\111\000\112\000\053\000\054\000\
\055\000\056\000\000\000\149\000\000\000\000\000\000\000\113\000\
\150\000\103\000\104\000\105\000\106\000\000\000\107\000\108\000\
\109\000\110\000\111\000\112\000\000\000\000\000\000\000\000\000\
\143\000\000\000\000\000\000\000\000\000\113\000\103\000\104\000\
\105\000\106\000\000\000\107\000\108\000\109\000\110\000\111\000\
\112\000\000\000\000\000\000\000\000\000\145\000\000\000\000\000\
\000\000\000\000\113\000\103\000\104\000\105\000\106\000\000\000\
\107\000\108\000\109\000\110\000\111\000\112\000\000\000\000\000\
\000\000\000\000\160\000\000\000\000\000\000\000\000\000\113\000\
\103\000\104\000\105\000\106\000\000\000\107\000\108\000\109\000\
\110\000\111\000\112\000\000\000\000\000\000\000\000\000\169\000\
\000\000\000\000\000\000\000\000\113\000\103\000\104\000\105\000\
\106\000\000\000\107\000\108\000\109\000\110\000\111\000\112\000\
\000\000\000\000\000\000\000\000\171\000\000\000\000\000\000\000\
\000\000\113\000\103\000\104\000\105\000\106\000\000\000\107\000\
\108\000\109\000\110\000\111\000\112\000\000\000\000\000\000\000\
\000\000\176\000\000\000\000\000\000\000\000\000\113\000\103\000\
\104\000\105\000\106\000\000\000\107\000\108\000\109\000\110\000\
\111\000\112\000\000\000\000\000\000\000\000\000\181\000\000\000\
\000\000\000\000\000\000\113\000\103\000\104\000\105\000\106\000\
\000\000\107\000\108\000\109\000\110\000\111\000\112\000\000\000\
\000\000\000\000\000\000\162\000\103\000\104\000\105\000\106\000\
\113\000\107\000\108\000\109\000\110\000\111\000\112\000\000\000\
\000\000\000\000\000\000\165\000\103\000\104\000\105\000\106\000\
\113\000\107\000\108\000\109\000\110\000\111\000\112\000\000\000\
\000\000\000\000\000\000\174\000\103\000\104\000\105\000\106\000\
\113\000\107\000\108\000\109\000\110\000\111\000\112\000\000\000\
\000\000\000\000\000\000\179\000\103\000\104\000\105\000\106\000\
\113\000\107\000\108\000\109\000\110\000\111\000\112\000\000\000\
\000\000\000\000\000\000\000\000\103\000\104\000\105\000\106\000\
\113\000\107\000\108\000\109\000\110\000\111\000\112\000\000\000\
\000\000\000\000\103\000\104\000\105\000\106\000\000\000\000\000\
\113\000\109\000\110\000\111\000\112\000\000\000\000\000\000\000\
\045\000\000\000\045\000\000\000\000\000\000\000\113\000\045\000\
\045\000\045\000\045\000\045\000\000\000\045\000\045\000\045\000\
\045\000\045\000\045\000\046\000\000\000\046\000\000\000\000\000\
\000\000\000\000\046\000\046\000\046\000\046\000\046\000\000\000\
\046\000\046\000\046\000\046\000\046\000\046\000\047\000\000\000\
\047\000\000\000\000\000\000\000\000\000\047\000\047\000\047\000\
\000\000\000\000\000\000\047\000\047\000\047\000\047\000\047\000\
\047\000\043\000\000\000\043\000\000\000\000\000\000\000\000\000\
\043\000\043\000\043\000\000\000\000\000\000\000\043\000\043\000\
\043\000\043\000\043\000\043\000\044\000\000\000\044\000\000\000\
\000\000\000\000\000\000\044\000\044\000\044\000\000\000\000\000\
\000\000\044\000\044\000\044\000\044\000\044\000\044\000\050\000\
\000\000\050\000\000\000\000\000\000\000\000\000\050\000\000\000\
\000\000\000\000\000\000\000\000\050\000\050\000\050\000\050\000\
\050\000\050\000\051\000\000\000\051\000\000\000\000\000\000\000\
\000\000\051\000\000\000\000\000\000\000\000\000\000\000\051\000\
\051\000\051\000\051\000\051\000\051\000\052\000\000\000\052\000\
\000\000\000\000\000\000\000\000\052\000\000\000\000\000\000\000\
\000\000\000\000\052\000\052\000\052\000\052\000\052\000\052\000\
\053\000\000\000\053\000\000\000\000\000\000\000\000\000\053\000\
\000\000\000\000\000\000\000\000\000\000\053\000\053\000\053\000\
\053\000\053\000\053\000\049\000\000\000\049\000\000\000\000\000\
\000\000\000\000\049\000\000\000\000\000\000\000\000\000\000\000\
\049\000\049\000"

let yycheck = "\071\000\
\001\001\003\001\003\001\075\000\076\000\144\000\008\001\008\001\
\009\001\010\001\011\001\012\001\001\000\014\001\015\001\016\001\
\017\001\018\001\019\001\002\001\159\000\001\001\002\001\095\000\
\096\000\097\000\098\000\099\000\029\001\034\001\013\001\103\000\
\104\000\105\000\106\000\107\000\108\000\109\000\110\000\111\000\
\112\000\113\000\002\001\115\000\004\001\028\001\006\001\007\001\
\031\001\002\001\010\001\004\001\034\001\006\001\007\001\034\001\
\001\001\010\001\003\001\034\001\020\001\021\001\034\001\023\001\
\024\001\011\001\012\001\020\001\021\001\002\001\023\001\024\001\
\144\000\033\001\034\001\147\000\148\000\143\000\150\000\145\000\
\033\001\034\001\034\001\029\001\156\000\034\001\002\001\159\000\
\004\001\001\001\162\000\003\001\158\000\165\000\010\001\034\001\
\008\001\034\001\003\001\003\001\172\000\167\000\174\000\008\001\
\002\001\177\000\004\001\179\000\006\001\007\001\003\001\002\001\
\010\001\004\001\008\001\006\001\007\001\033\001\034\001\010\001\
\001\001\002\001\020\001\021\001\003\001\023\001\024\001\001\001\
\002\001\020\001\021\001\006\001\023\001\024\001\003\001\033\001\
\034\001\009\001\010\001\011\001\012\001\003\001\033\001\034\001\
\002\001\006\001\004\001\006\001\006\001\007\001\006\001\002\001\
\010\001\004\001\006\001\006\001\007\001\029\001\034\001\010\001\
\001\001\002\001\020\001\021\001\034\001\023\001\024\001\034\001\
\034\001\020\001\021\001\001\001\023\001\024\001\001\001\033\001\
\034\001\053\000\054\000\055\000\056\000\001\001\033\001\034\001\
\002\001\034\001\004\001\001\001\006\001\007\001\002\001\002\001\
\010\001\004\001\002\001\006\001\007\001\073\000\002\001\010\001\
\005\001\008\001\020\001\021\001\001\001\023\001\024\001\003\001\
\034\001\020\001\021\001\034\001\023\001\024\001\008\001\033\001\
\034\001\002\001\002\001\004\001\029\001\006\001\033\001\034\001\
\002\001\010\001\019\000\001\001\021\000\003\001\023\000\024\000\
\034\001\022\001\008\001\020\001\021\001\001\001\023\001\024\001\
\014\001\015\001\003\001\001\001\000\000\003\001\002\001\002\001\
\033\001\034\001\008\001\009\001\010\001\011\001\012\001\003\001\
\014\001\015\001\016\001\017\001\018\001\019\001\003\001\005\001\
\001\001\025\001\026\001\027\001\003\001\005\001\030\001\029\001\
\009\001\010\001\011\001\012\001\001\001\014\001\015\001\016\001\
\017\001\018\001\019\001\003\000\003\001\001\001\025\001\026\001\
\027\001\003\001\041\000\030\001\029\001\009\001\010\001\011\001\
\012\001\255\255\014\001\015\001\016\001\017\001\018\001\019\001\
\255\255\025\001\026\001\027\001\255\255\003\001\030\001\255\255\
\032\001\029\001\008\001\009\001\010\001\011\001\012\001\255\255\
\014\001\015\001\016\001\017\001\018\001\019\001\048\000\049\000\
\050\000\051\000\255\255\003\001\255\255\255\255\255\255\029\001\
\008\001\009\001\010\001\011\001\012\001\255\255\014\001\015\001\
\016\001\017\001\018\001\019\001\255\255\255\255\255\255\255\255\
\003\001\255\255\255\255\255\255\255\255\029\001\009\001\010\001\
\011\001\012\001\255\255\014\001\015\001\016\001\017\001\018\001\
\019\001\255\255\255\255\255\255\255\255\003\001\255\255\255\255\
\255\255\255\255\029\001\009\001\010\001\011\001\012\001\255\255\
\014\001\015\001\016\001\017\001\018\001\019\001\255\255\255\255\
\255\255\255\255\003\001\255\255\255\255\255\255\255\255\029\001\
\009\001\010\001\011\001\012\001\255\255\014\001\015\001\016\001\
\017\001\018\001\019\001\255\255\255\255\255\255\255\255\003\001\
\255\255\255\255\255\255\255\255\029\001\009\001\010\001\011\001\
\012\001\255\255\014\001\015\001\016\001\017\001\018\001\019\001\
\255\255\255\255\255\255\255\255\003\001\255\255\255\255\255\255\
\255\255\029\001\009\001\010\001\011\001\012\001\255\255\014\001\
\015\001\016\001\017\001\018\001\019\001\255\255\255\255\255\255\
\255\255\003\001\255\255\255\255\255\255\255\255\029\001\009\001\
\010\001\011\001\012\001\255\255\014\001\015\001\016\001\017\001\
\018\001\019\001\255\255\255\255\255\255\255\255\003\001\255\255\
\255\255\255\255\255\255\029\001\009\001\010\001\011\001\012\001\
\255\255\014\001\015\001\016\001\017\001\018\001\019\001\255\255\
\255\255\255\255\255\255\008\001\009\001\010\001\011\001\012\001\
\029\001\014\001\015\001\016\001\017\001\018\001\019\001\255\255\
\255\255\255\255\255\255\008\001\009\001\010\001\011\001\012\001\
\029\001\014\001\015\001\016\001\017\001\018\001\019\001\255\255\
\255\255\255\255\255\255\008\001\009\001\010\001\011\001\012\001\
\029\001\014\001\015\001\016\001\017\001\018\001\019\001\255\255\
\255\255\255\255\255\255\008\001\009\001\010\001\011\001\012\001\
\029\001\014\001\015\001\016\001\017\001\018\001\019\001\255\255\
\255\255\255\255\255\255\255\255\009\001\010\001\011\001\012\001\
\029\001\014\001\015\001\016\001\017\001\018\001\019\001\255\255\
\255\255\255\255\009\001\010\001\011\001\012\001\255\255\255\255\
\029\001\016\001\017\001\018\001\019\001\255\255\255\255\255\255\
\001\001\255\255\003\001\255\255\255\255\255\255\029\001\008\001\
\009\001\010\001\011\001\012\001\255\255\014\001\015\001\016\001\
\017\001\018\001\019\001\001\001\255\255\003\001\255\255\255\255\
\255\255\255\255\008\001\009\001\010\001\011\001\012\001\255\255\
\014\001\015\001\016\001\017\001\018\001\019\001\001\001\255\255\
\003\001\255\255\255\255\255\255\255\255\008\001\009\001\010\001\
\255\255\255\255\255\255\014\001\015\001\016\001\017\001\018\001\
\019\001\001\001\255\255\003\001\255\255\255\255\255\255\255\255\
\008\001\009\001\010\001\255\255\255\255\255\255\014\001\015\001\
\016\001\017\001\018\001\019\001\001\001\255\255\003\001\255\255\
\255\255\255\255\255\255\008\001\009\001\010\001\255\255\255\255\
\255\255\014\001\015\001\016\001\017\001\018\001\019\001\001\001\
\255\255\003\001\255\255\255\255\255\255\255\255\008\001\255\255\
\255\255\255\255\255\255\255\255\014\001\015\001\016\001\017\001\
\018\001\019\001\001\001\255\255\003\001\255\255\255\255\255\255\
\255\255\008\001\255\255\255\255\255\255\255\255\255\255\014\001\
\015\001\016\001\017\001\018\001\019\001\001\001\255\255\003\001\
\255\255\255\255\255\255\255\255\008\001\255\255\255\255\255\255\
\255\255\255\255\014\001\015\001\016\001\017\001\018\001\019\001\
\001\001\255\255\003\001\255\255\255\255\255\255\255\255\008\001\
\255\255\255\255\255\255\255\255\255\255\014\001\015\001\016\001\
\017\001\018\001\019\001\001\001\255\255\003\001\255\255\255\255\
\255\255\255\255\008\001\255\255\255\255\255\255\255\255\255\255\
\014\001\015\001"

let yynames_const = "\
  SEMI\000\
  LPAREN\000\
  RPAREN\000\
  LBRACK\000\
  RBRACK\000\
  LBRACE\000\
  RBRACE\000\
  COMMA\000\
  PLUS\000\
  MINUS\000\
  TIMES\000\
  DIVIDE\000\
  ASSIGN\000\
  EQ\000\
  NEQ\000\
  LT\000\
  LEQ\000\
  GT\000\
  GEQ\000\
  RETURN\000\
  IF\000\
  ELSE\000\
  FOR\000\
  WHILE\000\
  INT\000\
  POINT\000\
  CURVE\000\
  DOT\000\
  TRANS\000\
  LAYER\000\
  ADDTOLAYER\000\
  VOID\000\
  EOF\000\
  "

let yynames_block = "\
  LITERAL\000\
  ID\000\
  "

let yyact = [|
  (fun _ -> failwith "parser")
; (fun __caml_parser_env ->
    Obj.repr(
# 27 "parser.mly"
                 ( [], [] )
# 438 "parser.ml"
               : Ast.program))
; (fun __caml_parser_env ->
    let _1 = (Parsing.peek_val __caml_parser_env 1 : Ast.program) in
    let _2 = (Parsing.peek_val __caml_parser_env 0 : 'vdecl) in
    Obj.repr(
# 28 "parser.mly"
                 ( (_2 :: fst _1), snd _1 )
# 446 "parser.ml"
               : Ast.program))
; (fun __caml_parser_env ->
    let _1 = (Parsing.peek_val __caml_parser_env 1 : Ast.program) in
    let _2 = (Parsing.peek_val __caml_parser_env 0 : 'fdecl) in
    Obj.repr(
# 29 "parser.mly"
                 ( fst _1, (_2 :: snd _1) )
# 454 "parser.ml"
               : Ast.program))
; (fun __caml_parser_env ->
    let _2 = (Parsing.peek_val __caml_parser_env 7 : string) in
    let _4 = (Parsing.peek_val __caml_parser_env 5 : 'formals_opt) in
    let _7 = (Parsing.peek_val __caml_parser_env 2 : 'vdecl_list) in
    let _8 = (Parsing.peek_val __caml_parser_env 1 : 'stmt_list) in
    Obj.repr(
# 33 "parser.mly"
     ( { return = Literalt;
         fname = _2;
         formals = _4;
         locals = List.rev _7;
         body = List.rev _8 } )
# 468 "parser.ml"
               : 'fdecl))
; (fun __caml_parser_env ->
    let _2 = (Parsing.peek_val __caml_parser_env 7 : string) in
    let _4 = (Parsing.peek_val __caml_parser_env 5 : 'formals_opt) in
    let _7 = (Parsing.peek_val __caml_parser_env 2 : 'vdecl_list) in
    let _8 = (Parsing.peek_val __caml_parser_env 1 : 'stmt_list) in
    Obj.repr(
# 39 "parser.mly"
     ( { return = Pointt;
         fname = _2;
         formals = _4;
         locals = List.rev _7;
         body = List.rev _8 } )
# 482 "parser.ml"
               : 'fdecl))
; (fun __caml_parser_env ->
    let _2 = (Parsing.peek_val __caml_parser_env 7 : string) in
    let _4 = (Parsing.peek_val __caml_parser_env 5 : 'formals_opt) in
    let _7 = (Parsing.peek_val __caml_parser_env 2 : 'vdecl_list) in
    let _8 = (Parsing.peek_val __caml_parser_env 1 : 'stmt_list) in
    Obj.repr(
# 45 "parser.mly"
     ( { return = Curvet;
         fname = _2;
         formals = _4;
         locals = List.rev _7;
         body = List.rev _8 } )
# 496 "parser.ml"
               : 'fdecl))
; (fun __caml_parser_env ->
    let _2 = (Parsing.peek_val __caml_parser_env 7 : string) in
    let _4 = (Parsing.peek_val __caml_parser_env 5 : 'formals_opt) in
    let _7 = (Parsing.peek_val __caml_parser_env 2 : 'vdecl_list) in
    let _8 = (Parsing.peek_val __caml_parser_env 1 : 'stmt_list) in
    Obj.repr(
# 51 "parser.mly"
     ( { return = Layert;
         fname = _2;
         formals = _4;
         locals = List.rev _7;
         body = List.rev _8 } )
# 510 "parser.ml"
               : 'fdecl))
; (fun __caml_parser_env ->
    let _2 = (Parsing.peek_val __caml_parser_env 7 : string) in
    let _4 = (Parsing.peek_val __caml_parser_env 5 : 'formals_opt) in
    let _7 = (Parsing.peek_val __caml_parser_env 2 : 'vdecl_list) in
    let _8 = (Parsing.peek_val __caml_parser_env 1 : 'stmt_list) in
    Obj.repr(
# 57 "parser.mly"
     ( { return = Voidt;
         fname = _2;
         formals = _4;
         locals = List.rev _7;
         body = List.rev _8 } )
# 524 "parser.ml"
               : 'fdecl))
; (fun __caml_parser_env ->
    Obj.repr(
# 64 "parser.mly"
                  ( [] )
# 530 "parser.ml"
               : 'formals_opt))
; (fun __caml_parser_env ->
    let _1 = (Parsing.peek_val __caml_parser_env 0 : 'formal_list) in
    Obj.repr(
# 65 "parser.mly"
                  ( List.rev _1 )
# 537 "parser.ml"
               : 'formals_opt))
; (fun __caml_parser_env ->
    let _1 = (Parsing.peek_val __caml_parser_env 0 : 'formal) in
    Obj.repr(
# 68 "parser.mly"
                             ( [_1] )
# 544 "parser.ml"
               : 'formal_list))
; (fun __caml_parser_env ->
    let _1 = (Parsing.peek_val __caml_parser_env 2 : 'formal_list) in
    let _3 = (Parsing.peek_val __caml_parser_env 0 : 'formal) in
    Obj.repr(
# 69 "parser.mly"
                             ( _3 :: _1 )
# 552 "parser.ml"
               : 'formal_list))
; (fun __caml_parser_env ->
    let _2 = (Parsing.peek_val __caml_parser_env 0 : string) in
    Obj.repr(
# 72 "parser.mly"
           ( { t = Literalt; name = _2; value = [0] } )
# 559 "parser.ml"
               : 'formal))
; (fun __caml_parser_env ->
    let _2 = (Parsing.peek_val __caml_parser_env 0 : string) in
    Obj.repr(
# 73 "parser.mly"
             ( { t = Pointt; name = _2; value = [0; 0] } )
# 566 "parser.ml"
               : 'formal))
; (fun __caml_parser_env ->
    let _2 = (Parsing.peek_val __caml_parser_env 0 : string) in
    Obj.repr(
# 74 "parser.mly"
             ( { t = Curvet; name = _2; value = [0; 0; 0; 0; 0; 0; 0; 0] } )
# 573 "parser.ml"
               : 'formal))
; (fun __caml_parser_env ->
    let _2 = (Parsing.peek_val __caml_parser_env 0 : string) in
    Obj.repr(
# 75 "parser.mly"
             ( { t = Layert; name = _2; value = [] } )
# 580 "parser.ml"
               : 'formal))
; (fun __caml_parser_env ->
    Obj.repr(
# 79 "parser.mly"
                     ( [] )
# 586 "parser.ml"
               : 'vdecl_list))
; (fun __caml_parser_env ->
    let _1 = (Parsing.peek_val __caml_parser_env 1 : 'vdecl_list) in
    let _2 = (Parsing.peek_val __caml_parser_env 0 : 'vdecl) in
    Obj.repr(
# 80 "parser.mly"
                     ( _2 :: _1 )
# 594 "parser.ml"
               : 'vdecl_list))
; (fun __caml_parser_env ->
    let _2 = (Parsing.peek_val __caml_parser_env 1 : string) in
    Obj.repr(
# 83 "parser.mly"
                ( { t = Literalt; name = _2; value = [0] } )
# 601 "parser.ml"
               : 'vdecl))
; (fun __caml_parser_env ->
    let _2 = (Parsing.peek_val __caml_parser_env 1 : string) in
    Obj.repr(
# 84 "parser.mly"
                  ( { t = Pointt; name = _2; value = [0; 0] } )
# 608 "parser.ml"
               : 'vdecl))
; (fun __caml_parser_env ->
    let _2 = (Parsing.peek_val __caml_parser_env 1 : string) in
    Obj.repr(
# 85 "parser.mly"
                  ( { t = Curvet; name = _2; value = [0; 0; 0; 0; 0; 0; 0; 0] } )
# 615 "parser.ml"
               : 'vdecl))
; (fun __caml_parser_env ->
    let _2 = (Parsing.peek_val __caml_parser_env 1 : string) in
    Obj.repr(
# 86 "parser.mly"
                  ( { t = Layert; name = _2; value = [] } )
# 622 "parser.ml"
               : 'vdecl))
; (fun __caml_parser_env ->
    Obj.repr(
# 89 "parser.mly"
                   ( [] )
# 628 "parser.ml"
               : 'stmt_list))
; (fun __caml_parser_env ->
    let _1 = (Parsing.peek_val __caml_parser_env 1 : 'stmt_list) in
    let _2 = (Parsing.peek_val __caml_parser_env 0 : 'stmt) in
    Obj.repr(
# 90 "parser.mly"
                   ( _2 :: _1 )
# 636 "parser.ml"
               : 'stmt_list))
; (fun __caml_parser_env ->
    let _1 = (Parsing.peek_val __caml_parser_env 1 : 'expr) in
    Obj.repr(
# 93 "parser.mly"
              ( Expr(_1) )
# 643 "parser.ml"
               : 'stmt))
; (fun __caml_parser_env ->
    let _2 = (Parsing.peek_val __caml_parser_env 1 : 'expr) in
    Obj.repr(
# 94 "parser.mly"
                     ( Return(_2) )
# 650 "parser.ml"
               : 'stmt))
; (fun __caml_parser_env ->
    let _2 = (Parsing.peek_val __caml_parser_env 1 : 'stmt_list) in
    Obj.repr(
# 95 "parser.mly"
                            ( Block(List.rev _2) )
# 657 "parser.ml"
               : 'stmt))
; (fun __caml_parser_env ->
    let _3 = (Parsing.peek_val __caml_parser_env 2 : 'expr) in
    let _5 = (Parsing.peek_val __caml_parser_env 0 : 'stmt) in
    Obj.repr(
# 96 "parser.mly"
                                            ( If(_3, _5, Block([])) )
# 665 "parser.ml"
               : 'stmt))
; (fun __caml_parser_env ->
    let _3 = (Parsing.peek_val __caml_parser_env 4 : 'expr) in
    let _5 = (Parsing.peek_val __caml_parser_env 2 : 'stmt) in
    let _7 = (Parsing.peek_val __caml_parser_env 0 : 'stmt) in
    Obj.repr(
# 97 "parser.mly"
                                            ( If(_3, _5, _7) )
# 674 "parser.ml"
               : 'stmt))
; (fun __caml_parser_env ->
    let _3 = (Parsing.peek_val __caml_parser_env 6 : 'expr_opt) in
    let _5 = (Parsing.peek_val __caml_parser_env 4 : 'expr_opt) in
    let _7 = (Parsing.peek_val __caml_parser_env 2 : 'expr_opt) in
    let _9 = (Parsing.peek_val __caml_parser_env 0 : 'stmt) in
    Obj.repr(
# 99 "parser.mly"
     ( For(_3, _5, _7, _9) )
# 684 "parser.ml"
               : 'stmt))
; (fun __caml_parser_env ->
    let _3 = (Parsing.peek_val __caml_parser_env 2 : 'expr) in
    let _5 = (Parsing.peek_val __caml_parser_env 0 : 'stmt) in
    Obj.repr(
# 100 "parser.mly"
                                  ( While(_3, _5) )
# 692 "parser.ml"
               : 'stmt))
; (fun __caml_parser_env ->
    Obj.repr(
# 103 "parser.mly"
                  ( Noexpr )
# 698 "parser.ml"
               : 'expr_opt))
; (fun __caml_parser_env ->
    let _1 = (Parsing.peek_val __caml_parser_env 0 : 'expr) in
    Obj.repr(
# 104 "parser.mly"
                  ( _1 )
# 705 "parser.ml"
               : 'expr_opt))
; (fun __caml_parser_env ->
    let _1 = (Parsing.peek_val __caml_parser_env 0 : int) in
    Obj.repr(
# 107 "parser.mly"
                     ( Literal(_1) )
# 712 "parser.ml"
               : 'expr))
; (fun __caml_parser_env ->
    let _1 = (Parsing.peek_val __caml_parser_env 5 : string) in
    let _3 = (Parsing.peek_val __caml_parser_env 3 : string) in
    let _5 = (Parsing.peek_val __caml_parser_env 1 : 'expr) in
    Obj.repr(
# 109 "parser.mly"
      ( Dotop(_1, _3, _5))
# 721 "parser.ml"
               : 'expr))
; (fun __caml_parser_env ->
    let _2 = (Parsing.peek_val __caml_parser_env 18 : 'expr) in
    let _4 = (Parsing.peek_val __caml_parser_env 16 : 'expr) in
    let _7 = (Parsing.peek_val __caml_parser_env 13 : 'expr) in
    let _9 = (Parsing.peek_val __caml_parser_env 11 : 'expr) in
    let _12 = (Parsing.peek_val __caml_parser_env 8 : 'expr) in
    let _14 = (Parsing.peek_val __caml_parser_env 6 : 'expr) in
    let _17 = (Parsing.peek_val __caml_parser_env 3 : 'expr) in
    let _19 = (Parsing.peek_val __caml_parser_env 1 : 'expr) in
    Obj.repr(
# 114 "parser.mly"
            ( Curve(_2, _4, _7, _9, _12, _14, _17, _19))
# 735 "parser.ml"
               : 'expr))
; (fun __caml_parser_env ->
    let _2 = (Parsing.peek_val __caml_parser_env 3 : 'expr) in
    let _4 = (Parsing.peek_val __caml_parser_env 1 : 'expr) in
    Obj.repr(
# 115 "parser.mly"
                                  ( Point(_2, _4))
# 743 "parser.ml"
               : 'expr))
; (fun __caml_parser_env ->
    let _2 = (Parsing.peek_val __caml_parser_env 7 : 'expr) in
    let _4 = (Parsing.peek_val __caml_parser_env 5 : 'expr) in
    let _6 = (Parsing.peek_val __caml_parser_env 3 : 'expr) in
    let _8 = (Parsing.peek_val __caml_parser_env 1 : 'expr) in
    Obj.repr(
# 116 "parser.mly"
                                                        ( Matrix(_2, _4, _6, _8) )
# 753 "parser.ml"
               : 'expr))
; (fun __caml_parser_env ->
    let _1 = (Parsing.peek_val __caml_parser_env 2 : 'expr) in
    let _3 = (Parsing.peek_val __caml_parser_env 0 : 'expr) in
    Obj.repr(
# 117 "parser.mly"
                     ( Trans(_1, _3) )
# 761 "parser.ml"
               : 'expr))
; (fun __caml_parser_env ->
    let _2 = (Parsing.peek_val __caml_parser_env 1 : 'cvs_opt) in
    Obj.repr(
# 118 "parser.mly"
                          ( Layer(_2) )
# 768 "parser.ml"
               : 'expr))
; (fun __caml_parser_env ->
    let _1 = (Parsing.peek_val __caml_parser_env 2 : string) in
    let _3 = (Parsing.peek_val __caml_parser_env 0 : string) in
    Obj.repr(
# 119 "parser.mly"
                            ( AddToLayer(_1, _3) )
# 776 "parser.ml"
               : 'expr))
; (fun __caml_parser_env ->
    let _1 = (Parsing.peek_val __caml_parser_env 0 : string) in
    Obj.repr(
# 120 "parser.mly"
                     ( Id(_1) )
# 783 "parser.ml"
               : 'expr))
; (fun __caml_parser_env ->
    let _1 = (Parsing.peek_val __caml_parser_env 2 : 'expr) in
    let _3 = (Parsing.peek_val __caml_parser_env 0 : 'expr) in
    Obj.repr(
# 121 "parser.mly"
                     ( Binop(_1, Add,   _3) )
# 791 "parser.ml"
               : 'expr))
; (fun __caml_parser_env ->
    let _1 = (Parsing.peek_val __caml_parser_env 2 : 'expr) in
    let _3 = (Parsing.peek_val __caml_parser_env 0 : 'expr) in
    Obj.repr(
# 122 "parser.mly"
                     ( Binop(_1, Sub,   _3) )
# 799 "parser.ml"
               : 'expr))
; (fun __caml_parser_env ->
    let _1 = (Parsing.peek_val __caml_parser_env 2 : 'expr) in
    let _3 = (Parsing.peek_val __caml_parser_env 0 : 'expr) in
    Obj.repr(
# 123 "parser.mly"
                     ( Binop(_1, Mult,  _3) )
# 807 "parser.ml"
               : 'expr))
; (fun __caml_parser_env ->
    let _1 = (Parsing.peek_val __caml_parser_env 2 : 'expr) in
    let _3 = (Parsing.peek_val __caml_parser_env 0 : 'expr) in
    Obj.repr(
# 124 "parser.mly"
                     ( Binop(_1, Div,   _3) )
# 815 "parser.ml"
               : 'expr))
; (fun __caml_parser_env ->
    let _2 = (Parsing.peek_val __caml_parser_env 0 : 'expr) in
    Obj.repr(
# 125 "parser.mly"
                     ( Binop(Literal(0),  Sub,   _2) )
# 822 "parser.ml"
               : 'expr))
; (fun __caml_parser_env ->
    let _1 = (Parsing.peek_val __caml_parser_env 2 : 'expr) in
    let _3 = (Parsing.peek_val __caml_parser_env 0 : 'expr) in
    Obj.repr(
# 126 "parser.mly"
                     ( Binop(_1, Equal, _3) )
# 830 "parser.ml"
               : 'expr))
; (fun __caml_parser_env ->
    let _1 = (Parsing.peek_val __caml_parser_env 2 : 'expr) in
    let _3 = (Parsing.peek_val __caml_parser_env 0 : 'expr) in
    Obj.repr(
# 127 "parser.mly"
                     ( Binop(_1, Neq,   _3) )
# 838 "parser.ml"
               : 'expr))
; (fun __caml_parser_env ->
    let _1 = (Parsing.peek_val __caml_parser_env 2 : 'expr) in
    let _3 = (Parsing.peek_val __caml_parser_env 0 : 'expr) in
    Obj.repr(
# 128 "parser.mly"
                     ( Binop(_1, Less,  _3) )
# 846 "parser.ml"
               : 'expr))
; (fun __caml_parser_env ->
    let _1 = (Parsing.peek_val __caml_parser_env 2 : 'expr) in
    let _3 = (Parsing.peek_val __caml_parser_env 0 : 'expr) in
    Obj.repr(
# 129 "parser.mly"
                     ( Binop(_1, Leq,   _3) )
# 854 "parser.ml"
               : 'expr))
; (fun __caml_parser_env ->
    let _1 = (Parsing.peek_val __caml_parser_env 2 : 'expr) in
    let _3 = (Parsing.peek_val __caml_parser_env 0 : 'expr) in
    Obj.repr(
# 130 "parser.mly"
                     ( Binop(_1, Greater,  _3) )
# 862 "parser.ml"
               : 'expr))
; (fun __caml_parser_env ->
    let _1 = (Parsing.peek_val __caml_parser_env 2 : 'expr) in
    let _3 = (Parsing.peek_val __caml_parser_env 0 : 'expr) in
    Obj.repr(
# 131 "parser.mly"
                     ( Binop(_1, Geq,   _3) )
# 870 "parser.ml"
               : 'expr))
; (fun __caml_parser_env ->
    let _1 = (Parsing.peek_val __caml_parser_env 2 : string) in
    let _3 = (Parsing.peek_val __caml_parser_env 0 : 'expr) in
    Obj.repr(
# 132 "parser.mly"
                     ( Assign(_1, _3) )
# 878 "parser.ml"
               : 'expr))
; (fun __caml_parser_env ->
    let _1 = (Parsing.peek_val __caml_parser_env 3 : string) in
    let _3 = (Parsing.peek_val __caml_parser_env 1 : 'actuals_opt) in
    Obj.repr(
# 133 "parser.mly"
                                 ( Call(_1, _3) )
# 886 "parser.ml"
               : 'expr))
; (fun __caml_parser_env ->
    let _2 = (Parsing.peek_val __caml_parser_env 1 : 'expr) in
    Obj.repr(
# 134 "parser.mly"
                       ( _2 )
# 893 "parser.ml"
               : 'expr))
; (fun __caml_parser_env ->
    Obj.repr(
# 137 "parser.mly"
                  ( [] )
# 899 "parser.ml"
               : 'actuals_opt))
; (fun __caml_parser_env ->
    let _1 = (Parsing.peek_val __caml_parser_env 0 : 'actuals_list) in
    Obj.repr(
# 138 "parser.mly"
                  ( List.rev _1 )
# 906 "parser.ml"
               : 'actuals_opt))
; (fun __caml_parser_env ->
    let _1 = (Parsing.peek_val __caml_parser_env 0 : 'expr) in
    Obj.repr(
# 141 "parser.mly"
                            ( [_1] )
# 913 "parser.ml"
               : 'actuals_list))
; (fun __caml_parser_env ->
    let _1 = (Parsing.peek_val __caml_parser_env 2 : 'actuals_list) in
    let _3 = (Parsing.peek_val __caml_parser_env 0 : 'expr) in
    Obj.repr(
# 142 "parser.mly"
                            ( _3 :: _1 )
# 921 "parser.ml"
               : 'actuals_list))
; (fun __caml_parser_env ->
    Obj.repr(
# 145 "parser.mly"
                  ( [] )
# 927 "parser.ml"
               : 'cvs_opt))
; (fun __caml_parser_env ->
    let _1 = (Parsing.peek_val __caml_parser_env 0 : 'cvs_list) in
    Obj.repr(
# 146 "parser.mly"
              ( List.rev _1 )
# 934 "parser.ml"
               : 'cvs_opt))
; (fun __caml_parser_env ->
    let _1 = (Parsing.peek_val __caml_parser_env 0 : string) in
    Obj.repr(
# 149 "parser.mly"
                          ( [_1] )
# 941 "parser.ml"
               : 'cvs_list))
; (fun __caml_parser_env ->
    let _1 = (Parsing.peek_val __caml_parser_env 2 : 'cvs_list) in
    let _3 = (Parsing.peek_val __caml_parser_env 0 : string) in
    Obj.repr(
# 150 "parser.mly"
                      ( _3 :: _1 )
# 949 "parser.ml"
               : 'cvs_list))
(* Entry program *)
; (fun __caml_parser_env -> raise (Parsing.YYexit (Parsing.peek_val __caml_parser_env 0)))
|]
let yytables =
  { Parsing.actions=yyact;
    Parsing.transl_const=yytransl_const;
    Parsing.transl_block=yytransl_block;
    Parsing.lhs=yylhs;
    Parsing.len=yylen;
    Parsing.defred=yydefred;
    Parsing.dgoto=yydgoto;
    Parsing.sindex=yysindex;
    Parsing.rindex=yyrindex;
    Parsing.gindex=yygindex;
    Parsing.tablesize=yytablesize;
    Parsing.table=yytable;
    Parsing.check=yycheck;
    Parsing.error_function=parse_error;
    Parsing.names_const=yynames_const;
    Parsing.names_block=yynames_block }
let program (lexfun : Lexing.lexbuf -> token) (lexbuf : Lexing.lexbuf) =
   (Parsing.yyparse yytables 1 lexfun lexbuf : Ast.program)
