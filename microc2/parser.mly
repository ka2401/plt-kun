%{ open Ast %}

%token SEMI LPAREN RPAREN LBRACK RBRACK LBRACE RBRACE COMMA
%token PLUS MINUS TIMES DIVIDE ASSIGN
%token EQ NEQ LT LEQ GT GEQ
%token RETURN IF ELSE FOR WHILE
%token INT POINT CURVE DOT TRANS LAYER ADDTOLAYER VOID
%token <int> LITERAL
%token <string> ID
%token EOF

%nonassoc NOELSE
%nonassoc ELSE
%right ASSIGN
%left EQ NEQ
%left LT GT LEQ GEQ
%left PLUS MINUS
%left TIMES DIVIDE
%left TRANS

%start program
%type <Ast.program> program

%%

program:
   /* nothing */ { [], [] }
 | program vdecl { ($2 :: fst $1), snd $1 }
 | program fdecl { fst $1, ($2 :: snd $1) }

fdecl:
   INT ID LPAREN formals_opt RPAREN LBRACE vdecl_list stmt_list RBRACE
     { { return = Literalt;
         fname = $2;
         formals = $4;
         locals = List.rev $7;
         body = List.rev $8 } }
 | POINT ID LPAREN formals_opt RPAREN LBRACE vdecl_list stmt_list RBRACE
     { { return = Pointt;
         fname = $2;
         formals = $4;
         locals = List.rev $7;
         body = List.rev $8 } }
 | CURVE ID LPAREN formals_opt RPAREN LBRACE vdecl_list stmt_list RBRACE
     { { return = Curvet;
         fname = $2;
         formals = $4;
         locals = List.rev $7;
         body = List.rev $8 } }
 | LAYER ID LPAREN formals_opt RPAREN LBRACE vdecl_list stmt_list RBRACE
     { { return = Layert;
         fname = $2;
         formals = $4;
         locals = List.rev $7;
         body = List.rev $8 } }
 | VOID ID LPAREN formals_opt RPAREN LBRACE vdecl_list stmt_list RBRACE
     { { return = Voidt;
         fname = $2;
         formals = $4;
         locals = List.rev $7;
         body = List.rev $8 } }

formals_opt:
    /* nothing */ { [] }
  | formal_list   { List.rev $1 }

formal_list:
    formal                   { [$1] }
  | formal_list COMMA formal { $3 :: $1 }

formal:
    INT ID { { t = Literalt; name = $2; value = [0] } }
  | POINT ID { { t = Pointt; name = $2; value = [0; 0] } }
  | CURVE ID { { t = Curvet; name = $2; value = [0; 0; 0; 0; 0; 0; 0; 0] } }
  | LAYER ID { { t = Layert; name = $2; value = [] } }


vdecl_list:
    /* nothing */    { [] }
  | vdecl_list vdecl { $2 :: $1 }

vdecl:
    INT ID SEMI { { t = Literalt; name = $2; value = [0] } }
  | POINT ID SEMI { { t = Pointt; name = $2; value = [0; 0] } }
  | CURVE ID SEMI { { t = Curvet; name = $2; value = [0; 0; 0; 0; 0; 0; 0; 0] } }
  | LAYER ID SEMI { { t = Layert; name = $2; value = [] } }

stmt_list:
    /* nothing */  { [] }
  | stmt_list stmt { $2 :: $1 }

stmt:
    expr SEMI { Expr($1) }
  | RETURN expr SEMI { Return($2) }
  | LBRACE stmt_list RBRACE { Block(List.rev $2) }
  | IF LPAREN expr RPAREN stmt %prec NOELSE { If($3, $5, Block([])) }
  | IF LPAREN expr RPAREN stmt ELSE stmt    { If($3, $5, $7) }
  | FOR LPAREN expr_opt SEMI expr_opt SEMI expr_opt RPAREN stmt
     { For($3, $5, $7, $9) }
  | WHILE LPAREN expr RPAREN stmt { While($3, $5) }

expr_opt:
    /* nothing */ { Noexpr }
  | expr          { $1 }

expr:
    LITERAL          { Literal($1) }
  | ID DOT ID LPAREN expr RPAREN
      { Dotop($1, $3, $5)}
  | LPAREN expr COMMA expr RPAREN
      LPAREN expr COMMA expr RPAREN
        LPAREN expr COMMA expr RPAREN
          LPAREN expr COMMA expr RPAREN 
            { Curve($2, $4, $7, $9, $12, $14, $17, $19)}
  | LPAREN expr COMMA expr RPAREN { Point($2, $4)}
  | LPAREN expr COMMA expr COMMA expr COMMA expr RPAREN { Matrix($2, $4, $6, $8) }
  | expr TRANS expr  { Trans($1, $3) }
  | LBRACK cvs_opt RBRACK { Layer($2) }
  | ID ADDTOLAYER ID        { AddToLayer($1, $3) }
  | ID               { Id($1) }
  | expr PLUS   expr { Binop($1, Add,   $3) }
  | expr MINUS  expr { Binop($1, Sub,   $3) }
  | expr TIMES  expr { Binop($1, Mult,  $3) }
  | expr DIVIDE expr { Binop($1, Div,   $3) }
  | MINUS  expr      { Binop(Literal(0),  Sub,   $2) }
  | expr EQ     expr { Binop($1, Equal, $3) }
  | expr NEQ    expr { Binop($1, Neq,   $3) }
  | expr LT     expr { Binop($1, Less,  $3) }
  | expr LEQ    expr { Binop($1, Leq,   $3) }
  | expr GT     expr { Binop($1, Greater,  $3) }
  | expr GEQ    expr { Binop($1, Geq,   $3) }
  | ID ASSIGN expr   { Assign($1, $3) }
  | ID LPAREN actuals_opt RPAREN { Call($1, $3) }
  | LPAREN expr RPAREN { $2 }

actuals_opt:
    /* nothing */ { [] }
  | actuals_list  { List.rev $1 }

actuals_list:
    expr                    { [$1] }
  | actuals_list COMMA expr { $3 :: $1 }

cvs_opt:
    /* nothing */ { [] }
  | cvs_list  { List.rev $1 }

cvs_list:
    ID                    { [$1] }
  | cvs_list COMMA ID { $3 :: $1 }

