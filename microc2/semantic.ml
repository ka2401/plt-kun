open Ast

module NameMap = Map.Make(struct
  type t = string
  let compare x y = Pervasives.compare x y
end)

exception ReturnException of curvet * var_decl NameMap.t



(* Main entry point: check a program *)

let rec check_layer cvs env = 
  let locals, globals = env in
  match cvs with
  | [] -> true
  | hd :: tl -> 
      if NameMap.mem hd locals && (NameMap.find hd locals).t = Curvet then
        check_layer tl env
      else if NameMap.mem hd globals && (NameMap.find hd globals).t = Curvet then
        check_layer tl env
      else false

let check_semantic (vars, funcs) =
  (* Put function declarations in a symbol table *)
  let func_decls = List.fold_left
      (fun funcs fdecl -> NameMap.add fdecl.fname fdecl funcs)
      NameMap.empty funcs
  in

  (* Invoke a function and return an updated global symbol table *)
  let rec call fdecl actuals globals =

    (* Evaluate an expression and return (value, updated environment) *)
    let rec eval env = function
        Literal(i) -> Literalt, env;
      | Dotop(var, op, e) -> Literalt, env
      | Curve(e1, e2, e3, e4, e5, e6, e7, e8) ->
    let l1, env = eval env e1 in
      let l2, env = eval env e2 in
        let l3, env = eval env e3 in
          let l4, env = eval env e4 in
            let l5, env = eval env e5 in
              let l6, env = eval env e6 in
                let l7, env = eval env e7 in
                  let l8, env = eval env e8 in
                    if List.map (fun(x) -> if x = Literalt then 1 else 0) [l1;l2;l3;l4;l5;l6;l7;l8] = [1;1;1;1;1;1;1;1] then
                      Curvet, env
                    else raise (Failure ("Invalid curve definition."))
      | Point(e1, e2) -> 
    let l1, env = eval env e1 in
      let l2, env = eval env e2 in
        if l1 = Literalt && l2 = Literalt then
          Pointt, env
        else raise (Failure ("Invalid point definition"))
      | Matrix(e1, e2, e3, e4) ->
    let l1, env = eval env e1 in
      let l2, env = eval env e2 in
        let l3, env = eval env e3 in
          let l4, env = eval env e4 in
            if List.map (fun(x)->if x = Literalt then 1 else 0) [l1;l2;l3;l4] = [1;1;1;1] then
              Matrixt, env
            else raise (Failure ("Invalid matrix definition"))
      | Trans(e1, e2) -> 
    let l1, env = eval env e1 in
      let l2, env = eval env e2 in
        if (l1 = Pointt || l1 = Curvet) && (l2 = Pointt || l2 = Matrixt) then
          l1, env
        else raise (Failure ("illegal trans operation"))
      | Layer(cvs) ->
    if check_layer cvs env then
      Layert, env
    else raise (Failure ("Invalid layer definition"))
      | AddToLayer(cv, ly) ->
    let locals, globals = env in 
      if NameMap.mem cv locals then
        if (NameMap.find cv locals).t = Curvet then
          if NameMap.mem ly locals then
            if (NameMap.find ly locals).t = Layert then
              Layert, env
            else raise (Failure (ly^" is not a Layer!"))
          else if NameMap.mem ly globals then
            if (NameMap.find ly globals).t = Layert then
              Layert, env
            else raise (Failure (ly^" is not a Layer!"))          
          else raise (Failure ("Undeclared variable "^ly))
        else raise (Failure (cv^" is not a Curve!"))
      else if NameMap.mem cv globals then
        if (NameMap.find cv globals).t = Curvet then
          if NameMap.mem ly locals then
            if (NameMap.find ly locals).t = Layert then
              Layert, env
            else raise (Failure (ly^" is not a Layer!"))
          else if NameMap.mem ly globals then
            if (NameMap.find ly globals).t = Layert then
              Layert, env
            else raise (Failure (ly^" is not a Layer!"))          
          else raise (Failure ("Undeclared variable "^ly))
        else raise (Failure (cv^" is not a Curve!"))
      else raise (Failure ("Undeclared variable "^cv))
      | Noexpr -> Literalt, env (* must be non-zero for the for loop predicate *)
      | Id(var) ->
    let locals, globals = env in
    if NameMap.mem var locals then
      (NameMap.find var locals).t, env
    else if NameMap.mem var globals then
      (NameMap.find var globals).t, env
    else raise (Failure ("Undeclared identifier " ^ var))
      | Binop(e1, op, e2) ->
    let l1, env = eval env e1 in
      let l2, env = eval env e2 in
        if l1 = Literalt && l1 = l2 then
          Literalt, env
        else raise (Failure ("Parameter should be integer for binary operation"))
      | Assign(var, e) ->
    let l, (locals, globals) = eval env e in
    if NameMap.mem var locals then
      if (NameMap.find var locals).t = l then
        l, (locals, globals)
      else raise (Failure ("Type mismatch for "^var))
    else if NameMap.mem var globals then
      if (NameMap.find var globals).t = l then
        l, (locals, globals)
      else raise (Failure ("Type mismatch for "^var))
    else raise (Failure ("Undeclared identifier " ^ var))
      | Call("draw", [e]) ->
    let l, env = eval env e in
      if l = Layert then
        (Literalt, env)
      else raise (Failure ("The parameter of draw() function should be Layer"))
      | Call(f, actuals) ->
    let fdecl =
      try NameMap.find f func_decls;
      with Not_found -> raise (Failure ("Undefined function " ^ f))
    in
    let actuals, env = List.fold_left
        (fun (actuals, env) actual ->
    let v, env = eval env actual in v :: actuals, env)
          ([], env) (List.rev actuals)
    in
    let (locals, globals) = env in
    try
      let globals = call fdecl actuals globals
      in Literalt, (locals, globals)
    with ReturnException(v, globals) -> v, (locals, globals)
    in

    (* Execute a statement and return an updated environment *)
    let rec exec env = function
  Block(stmts) -> List.fold_left exec env stmts
      | Expr(e) -> let _, env = eval env e in env
      | If(e, s1, s2) ->
    let l, env = eval env e in
      if l = Literalt then
        env
      else raise (Failure ("Invalid conditional statement."))
      | While(e, s) ->
    let l, env = eval env e in
      if l = Literalt then
        env
      else raise (Failure ("Invalid conditional statement."))
      | For(e1, e2, e3, s) ->
    let _, env = eval env e1 in
      let l, env = eval env e2 in
        if l = Literalt then
          let _, env = eval (exec env s) e3 in
          env
        else raise (Failure ("Invalid conditional statement."))
      | Return(e) ->
    let v, (locals, globals) = eval env e in
    raise (ReturnException(v, globals))
    in

    (* Enter the function: bind actual values to formal arguments *)
    let locals =
      try List.fold_left2
    (fun locals formal actual -> NameMap.add formal.name { t = formal.t; name = formal.name; value = [0] } locals)
    NameMap.empty fdecl.formals actuals
      with Invalid_argument(_) ->
  raise (Failure ("wrong number of arguments passed to " ^ fdecl.fname))
    in
    (* Initialize local variables to 0 *)
    let locals = List.fold_left
  (fun locals local -> NameMap.add local.name { t = local.t; name = local.name; value = local.value } locals) locals fdecl.locals
    in
    (* Execute each statement in sequence, return updated global symbol table *)
    snd (List.fold_left exec (locals, globals) fdecl.body)

  (* Run a program: initialize global variables to 0, find and run "main" *)
  in let globals = List.fold_left
      (fun globals vdecl -> NameMap.add vdecl.name { t = vdecl.t; name = vdecl.name; value = vdecl.value } globals) NameMap.empty vars
  in try
    call (NameMap.find "main" func_decls) [] globals
  with Not_found -> raise (Failure ("did not find the main() function"))
