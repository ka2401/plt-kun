int sum(int a, int b) {
  return a + b;
}

Point getPoint(int x, int y) {
  return (x, y);
}

void main()
{
  int i;
  Point p;
  Curve c;
  Layer l;

  p = (6, 3);
  draw(p);
  print(i = 5);
  print(sum(i, 2));
  p = (i + 1, sum(10, 2));
  draw(p);
  draw(getPoint(7, 9));
  for (i = 0; i < 10; i = i + 1) {
    draw((i, i * 2));
  }
  draw((1,2)(3,4)(5,6)(7,8));
  c = (1, 4)(15, 21)(10, 17)(11, 32);
  print(c.nth(2));
  print(c.getX(1));
  print(c.getY(1));
  draw(c);
  draw(c >> (2, 1, -1, 2));

  print(1---5);

  draw(p);
  for (i = 0; i < 4; i = i + 1) {
    draw(p = p >> (0, 1, -1, 0));
  }

  l = [abc, abdd, haha];
  l = aaa::l;

  drawLayer(l);
}
